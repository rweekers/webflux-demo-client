# Webflux demo client

Very small demo application with communicates with https://gitlab.com/rweekers/webflux-demo and acts as a simplified chat application. You can also run this as a Docker image (https://cloud.docker.com/u/rweekers/repository/docker/rweekers/webflux-client).